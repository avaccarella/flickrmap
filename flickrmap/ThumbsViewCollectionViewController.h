//
//  ThumbsViewCollectionViewController.h
//  flickrmap
//
//
//

#import <UIKit/UIKit.h>
#import "HorizontalLayout.h"


@interface ThumbsViewCollectionViewController : UICollectionViewController
@property (strong, nonatomic) IBOutlet HorizontalLayout *horizontalLayout;

@property (strong) NSDictionary* items; // itemid : paths (thumbpath, itempath, itemurl)

@end
