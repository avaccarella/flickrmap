//
//  FMDatasource.h
//  flickrmap
//
//
//

#import <Foundation/Foundation.h>
#import "FMAnnotation.h"

@protocol FMDatasourceDelegate<NSObject>

-(void)drawAllVisibleThumbs;
-(void)drawThumbnail:(FMAnnotation*)ann;

@end

@interface FMDatasource : NSObject
@property (weak) id<FMDatasourceDelegate> delegate;
- (NSArray*) getPhotos;
- (void) dropAllItems;
@end

@interface FMPhoto : NSObject
@property NSString* title;
@property NSString* photoId;
@property NSString* thumbPath;
@property NSString* fullItemPath;
@property NSString* fullItemUrl;
@property NSString* thumbUrl;
@property double latitude;
@property double longitude;

-(FMAnnotation*) createAnnotationForPhotoId:(NSString*)photoId;

@end