//
//  ViewController.m
//  flickrmap
//
//
//

#import "MapViewController.h"
#import "FMNetworkManager.h"
#import <CCHMapClusterController.h>
#import <CCHMapClusterAnnotation.h>
#import <CCHMapClusterControllerDelegate.h>
#import <WYPopoverController.h>
#import "ThumbsViewCollectionViewController.h"

@interface MapViewController()<MKMapViewDelegate,FMDatasourceDelegate,CCHMapClusterControllerDelegate>
@property FMDatasource* datasource;
@property NSMutableDictionary* itemsOnMap;//keep track of annotations already on map
@property (strong, nonatomic) CCHMapClusterController *mapClusterController;

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
@property WYPopoverController *callout;

@end

@implementation MapViewController
@synthesize datasource;

- (void)viewDidLoad {
    [super viewDidLoad];

    self.datasource = [[FMDatasource alloc] init];
    [self.datasource setDelegate:self];

    self.itemsOnMap = [[NSMutableDictionary alloc] init];
    
    self.mapView.delegate = self;
    
    self.mapClusterController = [[CCHMapClusterController alloc] initWithMapView:self.mapView];
    self.mapClusterController.debuggingEnabled = NO;
    self.mapClusterController.cellSize = 120;
    self.mapClusterController.delegate = self;

    //default location: corso indipendenza 5, Milan
    double defaultLatitude  = 45.46831;
    double defaultLongitude = 9.21211;
    
    CLLocation *defaultLocation = [[CLLocation alloc] initWithLatitude:defaultLatitude
                                                             longitude:defaultLongitude];
    
    [self centerMapOnLocation:defaultLocation];
    
    UIBarButtonItem* button = [[UIBarButtonItem alloc] initWithTitle:@"Map Type" style:UIBarButtonItemStylePlain target:self action:@selector(toggleMapType)];
    self.toolbar.items = @[button];
 
}

- (void) toggleMapType {
    if (self.mapView.mapType == MKMapTypeStandard) {
        self.mapView.mapType = MKMapTypeSatellite;
    } else {
        self.mapView.mapType = MKMapTypeStandard;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [self releaseMemory];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[FMNetworkManager getInstance] requestMetadataForBoundingBox:[self getBoundingBoxString]];
}

-(void) centerMapOnLocation:(CLLocation*) location {
    CLLocationDistance radius = 500;
    MKCoordinateRegion coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                              radius * 2.0, radius * 2.0);
    [self.mapView setRegion:coordinateRegion animated:YES];
}

#pragma mark -
#pragma mark FMDatasource delegate methods

-(void)drawThumbnail:(FMAnnotation *)ann {
    if(ann) {
        NSArray* annotations = [NSArray arrayWithObject:ann];
        [self.mapClusterController addAnnotations:annotations withCompletionHandler:nil];
    }
}

-(void)drawAllVisibleThumbs {
    NSArray* photos = [self.datasource getPhotos];
    NSMutableArray* annotations = [NSMutableArray array];
    for(FMPhoto* photo in photos) {
        if([self.itemsOnMap objectForKey:photo.photoId] == nil) {
            FMAnnotation* ann = [photo createAnnotationForPhotoId:photo.photoId];
            if(ann.thumbPath.length > 0) {
                [annotations addObject:ann];
                [self.itemsOnMap setObject:@"onmap" forKey:photo.photoId];
            }
        }
    }
    [self.mapClusterController addAnnotations:annotations withCompletionHandler:NULL];
}

#pragma mark -
#pragma mark MKMapView delegate methods

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
    [[FMNetworkManager getInstance] requestMetadataForBoundingBox:[self getBoundingBoxString]];
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    // Our popover handles deselection so tell the map view we'll take care of it
    [mapView deselectAnnotation:view.annotation animated:YES];
    
    if([view isKindOfClass:MKPinAnnotationView.class]) {
        
        // Start up our view controller from a Storyboard
        ThumbsViewCollectionViewController* controller = (ThumbsViewCollectionViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"ClusterPopoverIdentifier"];
        
        CCHMapClusterAnnotation *clusterAnnotation = (CCHMapClusterAnnotation *)view.annotation;
        NSSet* annotations = clusterAnnotation.annotations;
        NSMutableDictionary* itemsDictionary = [[NSMutableDictionary alloc] initWithCapacity:annotations.count];
        for(FMAnnotation* ann in annotations) {
            NSMutableDictionary* paths = [[NSMutableDictionary alloc] init];
            if(ann.thumbPath) {
                [paths setObject:ann.thumbPath forKey:@"thumbpath"];
            }
            if(ann.fullItemPath) {
                [paths setObject:ann.fullItemPath forKey:@"itempath"];
            }
            if(ann.fullItemUrl) {
                [paths setObject:ann.fullItemUrl forKey:@"itemurl"];
            }
            [itemsDictionary setObject:paths forKey:ann.itemId];
        }
        
        // Adjust this property to change the size of the popover + content
        CGFloat width = annotations.count > 3 ? 245 : (75.0*annotations.count + (10.0*annotations.count-1));
        controller.preferredContentSize = CGSizeMake(width, 75);
        
        controller.items = itemsDictionary;

        self.callout = [[WYPopoverController alloc] initWithContentViewController:controller];
        WYPopoverArrowDirection arrowDirection = WYPopoverArrowDirectionUp;
        if(view.frame.origin.y > [UIScreen mainScreen].bounds.size.height/2.0) {
            arrowDirection = WYPopoverArrowDirectionDown;
        }
        
        [self.callout presentPopoverFromRect:view.bounds inView:view permittedArrowDirections:arrowDirection animated:TRUE  options:WYPopoverAnimationOptionFadeWithScale];
    }
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {

    MKAnnotationView *view;
    NSString *identifier = nil;
    
    if ([annotation isKindOfClass:CCHMapClusterAnnotation.class]) {
        //this is a cluster
        
        identifier = @"clusterAnnotation";
        MKPinAnnotationView *clusterAnnotationView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (clusterAnnotationView) {
            clusterAnnotationView.annotation = annotation;
        } else {
            clusterAnnotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            clusterAnnotationView.canShowCallout = NO;
        }
        
        view = clusterAnnotationView;
    }
    return view;
}

#pragma mark -
#pragma mark utilities

- (NSString*) getBoundingBoxString {
    
    [self.mapView viewForAnnotation:nil];
    MKCoordinateRegion region = [self.mapView region];
    
    double maxLatitude  = region.center.latitude + region.span.latitudeDelta/2;
    double minLatitude  = region.center.latitude - region.span.latitudeDelta/2;
    
    double maxLongitude = region.center.longitude + region.span.longitudeDelta/2;
    double minLongitude = region.center.longitude - region.span.longitudeDelta/2;
    
    NSString* bbox = [NSString stringWithFormat:@"%f,%f,%f,%f",minLongitude,minLatitude,maxLongitude,maxLatitude];
    return bbox;
}

-(void) releaseMemory {
    [self.mapClusterController removeAnnotations:self.mapClusterController.annotations.allObjects withCompletionHandler:nil];
    [self.itemsOnMap removeAllObjects];
    [self.datasource dropAllItems];
}

@end
