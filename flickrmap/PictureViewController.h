//
//  PictureViewController.h
//  flickrmap
//
//
//

#import <UIKit/UIKit.h>
#import "FMDatasource.h"

@interface PictureViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property FMPhoto* photo;
@property (strong, nonatomic) IBOutlet UILabel *loadingLabel;
- (IBAction)backButtonPressed:(UIButton *)sender;
@end
