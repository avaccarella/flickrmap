//
//  HorizontalLayout.h
//  flickrmap
//
//
//

#import <UIKit/UIKit.h>

@interface HorizontalLayout : UICollectionViewFlowLayout

@property (nonatomic) UIEdgeInsets itemInsets;
@property (nonatomic) CGSize itemSize;
@property (nonatomic) CGFloat interItemSpacingX;
@property (nonatomic, strong) NSDictionary *layoutInfo;

@end
