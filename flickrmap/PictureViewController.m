//
//  PictureViewController.m
//  flickrmap
//
//
//

#import "PictureViewController.h"
#import "constants.h"
#import "FMNetworkManager.h"

@interface PictureViewController ()

@end

@implementation PictureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //listen for item download completion
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newFullItemCallback:) name:NEW_FULL_ITEM_NOTIFICATION object:nil];

    [self setup];
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) setup {
    
    BOOL isDir;
    if([[NSFileManager defaultManager] fileExistsAtPath:self.photo.fullItemPath isDirectory:&isDir]) {
        [self showImage];
    } else {
        //download item
        [[FMNetworkManager getInstance] downloadFullItem:self.photo];
    }
}

-(void) newFullItemCallback:(NSNotification*)notification {
    NSDictionary* info  = [notification userInfo];
    NSString* itemId    = [info objectForKey:@"id"];
    if([itemId isEqualToString:self.photo.photoId]) {
        [self showImage];
    }
}

- (void) showImage {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImage* img = [[UIImage alloc] initWithContentsOfFile:self.photo.fullItemPath];
        self.image.image = img;
        [self.loadingLabel setText:@""];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonPressed:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
