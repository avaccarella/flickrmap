//
//  HorizontalLayout.m
//  flickrmap
//
//
//

#import "HorizontalLayout.h"

@implementation HorizontalLayout

@synthesize itemInsets;
@synthesize itemSize;
@synthesize interItemSpacingX;
@synthesize layoutInfo;

-(id)init {
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.itemInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    self.itemSize = CGSizeMake(75.0f, 75.0f);
    self.interItemSpacingX = 10.0;
}

- (void)prepareLayout
{    
    NSMutableDictionary *cellLayoutInfo = [NSMutableDictionary dictionary];
    
    NSInteger itemCount = [self.collectionView numberOfItemsInSection:0];
    
    for (NSInteger item = 0; item < itemCount; item++) {
        NSIndexPath* indexPath = [NSIndexPath indexPathForItem:item inSection:0];
        
        UICollectionViewLayoutAttributes *itemAttributes =
        [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
        itemAttributes.frame = [self frameForAlbumPhotoAtIndexPath:indexPath];
        
        cellLayoutInfo[indexPath] = itemAttributes;
    }
    
    self.layoutInfo = cellLayoutInfo;
}

- (CGRect)frameForAlbumPhotoAtIndexPath:(NSIndexPath *)indexPath
{

    NSInteger column = indexPath.item;
    CGFloat spacingX = 10.0;    
    CGFloat originX  = floorf(self.itemInsets.left + (self.itemSize.width + spacingX) * column);    
    CGFloat originY  = self.itemInsets.top;
    
    return CGRectMake(originX, originY, self.itemSize.width, self.itemSize.height);
}


- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSMutableArray *allAttributes = [NSMutableArray arrayWithCapacity:self.layoutInfo.count];
    
    [self.layoutInfo enumerateKeysAndObjectsUsingBlock:^(NSIndexPath *indexPath,
                                                         UICollectionViewLayoutAttributes *attributes,
                                                         BOOL *innerStop) {
        if (CGRectIntersectsRect(rect, attributes.frame)) {
            [allAttributes addObject:attributes];
        }
    }];


    return allAttributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.layoutInfo[indexPath];
}

- (CGSize)collectionViewContentSize
{
    
    NSUInteger items = [self.collectionView numberOfItemsInSection:0];
    
    CGFloat height = self.itemInsets.top + self.itemInsets.bottom + self.itemSize.height;
    CGFloat width  = self.itemInsets.left + (self.itemSize.width+self.interItemSpacingX)*items + self.itemInsets.right;
    return CGSizeMake(width, height);
}

@end
