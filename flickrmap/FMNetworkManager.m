//
//  FMNetworkManager.m
//  flickrmap
//
//
//

#import "FMNetworkManager.h"
#import "FMDatasource.h"
#import "constants.h"

NSString* const apikey          = @"3ea47db656e804f11aceb188df75ea0e";
NSString* const apisecret       = @"37143f125db88619";
NSString* const fullItemSizeUrl    = @"url_z";//medium size preview

@interface  FMNetworkManager()
@property (strong) OFFlickrAPIContext* apiContext;
@property (strong) OFFlickrAPIRequest *flickrRequest;
@end

static FMNetworkManager* instance = nil;

@implementation FMNetworkManager
@synthesize apiContext;
@synthesize flickrRequest;
@synthesize nsUrlSession;

+ (FMNetworkManager*)getInstance {
    if (instance == nil) {
        instance = [[FMNetworkManager alloc] init];
    }
    return instance;
}

- (id) init {
    if (self = [super init]) {
        
        //init directory for downloaded thumbs
        NSString* thumbsFolder = [self createItemPath:nil isThumbnail:YES];
        [[NSFileManager defaultManager] createDirectoryAtPath:thumbsFolder withIntermediateDirectories:NO attributes:nil error:nil];
        
        //init directory for downloaded items
        NSString* itemsFolder = [self createItemPath:nil isThumbnail:NO];
        [[NSFileManager defaultManager] createDirectoryAtPath:itemsFolder withIntermediateDirectories:NO attributes:nil error:nil];
        
        self.apiContext = [[OFFlickrAPIContext alloc] initWithAPIKey:apikey
                                                        sharedSecret:apisecret];
        
        self.flickrRequest = [[OFFlickrAPIRequest alloc] initWithAPIContext:self.apiContext];
        [self.flickrRequest setDelegate:self];
        
        NSURLSessionConfiguration* def = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        self.nsUrlSession = [NSURLSession sessionWithConfiguration:def delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    }
    return self;
}

-(void)downloadThumbnail:(FMPhoto *)photo {
    
    BOOL isDir = NO;
    if([[NSFileManager defaultManager] fileExistsAtPath:photo.thumbPath isDirectory:&isDir]) {
        //Thumb already downloaded
        return;
    }

    NSURL* thumbPath = [NSURL fileURLWithPath:photo.thumbPath];
    NSURL* url = [NSURL URLWithString:photo.thumbUrl];
    NSString* photoId = photo.photoId;
    NSURLSessionDownloadTask* task = [self.nsUrlSession downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        if(!error) {
            NSError* moveErr = nil;
            [[NSFileManager defaultManager] moveItemAtURL:location toURL:thumbPath error:&moveErr];
            if(moveErr) {
                NSLog(@"ERROR moving thumb %@", moveErr.description);
            } else {
                //notify new thumb available
                NSDictionary* info = @{@"id":photoId};
                [[NSNotificationCenter defaultCenter] postNotificationName:NEW_THUMBNAIL_NOTIFICATION object:nil userInfo:info];
            }
        }
    }];
    task.priority = NSURLSessionTaskPriorityLow;
    [task resume];
}

-(void)downloadFullItem:(FMPhoto *)photo {
    
    BOOL isDir = NO;
    if([[NSFileManager defaultManager] fileExistsAtPath:photo.fullItemPath isDirectory:&isDir]) {
        //Item already downloaded
        return;
    }
    
    NSURL* itemPath = [NSURL fileURLWithPath:photo.fullItemPath];
    NSURL* url = [NSURL URLWithString:photo.fullItemUrl];
    NSString* photoId = photo.photoId;
    NSURLSessionDownloadTask* task = [self.nsUrlSession downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        if(!error) {
            NSError* moveErr = nil;
            [[NSFileManager defaultManager] moveItemAtURL:location toURL:itemPath error:&moveErr];
            if(moveErr) {
                NSLog(@"ERROR moving item %@", moveErr.description);
            } else {
                //notify new item available
                NSDictionary* info = @{@"id":photoId};
                [[NSNotificationCenter defaultCenter] postNotificationName:NEW_FULL_ITEM_NOTIFICATION object:nil userInfo:info];
            }
        }
    }];
    
    //full items download take priority over thumbnails
    task.priority = NSURLSessionTaskPriorityHigh;
    [task resume];
}

#pragma mark -
#pragma mark flickr api methods

-(void)requestMetadataForBoundingBox:(NSString *)bbox {
    [self requestMetadataForBoundingBox:bbox withPagination:1];
}

-(void)requestMetadataForBoundingBox:(NSString *)bbox withPagination:(NSInteger)pageNumber {
    
    NSString* page = [NSString stringWithFormat:@"%ld", pageNumber];
    
    NSMutableDictionary* requestArguments = [[NSMutableDictionary alloc] initWithCapacity:4];
    [requestArguments setObject:@"1"    forKey:@"accuracy"];
    [requestArguments setObject:bbox    forKey:@"bbox"];
    [requestArguments setObject:page    forKey:@"page"];
    NSString* extras = [NSString stringWithFormat:@"geo,url_sq,%@",fullItemSizeUrl];
    [requestArguments setObject:extras  forKey:@"extras"];

    [self.flickrRequest callAPIMethodWithGET:@"flickr.photos.search"
                                   arguments:requestArguments];

    self.lastBbox = bbox;
}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didCompleteWithResponse:(NSDictionary *)inResponseDictionary {
    
    //make sure we don't block the main thread while looping through items
    [self performSelectorInBackground:@selector(parseData:) withObject:inResponseDictionary];

    NSDictionary* result = [inResponseDictionary objectForKey:@"photos"];
    NSInteger pages      = [[result objectForKey:@"pages"] integerValue];
    NSInteger page       = [[result objectForKey:@"page"] integerValue];

    if(pages > 1 && page+1<=pages) {
        //ask for another page of data
        [self requestMetadataForBoundingBox:self.lastBbox withPagination:page+1];
    }
}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didFailWithError:(NSError *)inError {
    NSLog(@"response %@", inError);
}

#pragma mark -
#pragma mark utility methods

- (NSString*) createItemPath:(NSDictionary*)itemInfo isThumbnail:(BOOL)isThumb {
    NSArray*  paths         = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDir  = [paths objectAtIndex:0];
    NSString* itemName = [[itemInfo objectForKey:(isThumb ? @"url_sq" : fullItemSizeUrl)] lastPathComponent];
    NSString* subfolder = isThumb ? @"thumbs" : @"fullitems";
    NSString* itemPath = [[documentsDir stringByAppendingPathComponent:subfolder] stringByAppendingPathComponent:itemName];
    
    return itemPath;
}

-(void) parseData:(NSDictionary*)response {
    
    NSDictionary* result = [response objectForKey:@"photos"];
    NSInteger pages      = [[result objectForKey:@"pages"] integerValue];
    NSInteger page       = [[result objectForKey:@"page"] integerValue];
    NSArray* photos      = [result objectForKey:@"photo"];
    
    //build a map of FMPhoto
    NSMutableDictionary* FMPhotoMap = [[NSMutableDictionary alloc] initWithCapacity:photos.count];
    for (NSDictionary* photoInfo in photos) {
        FMPhoto* photo = [[FMPhoto alloc] init];
        NSString* photoId = [photoInfo objectForKey:@"id"];
        [photo setPhotoId:photoId];
        [photo setTitle:[photoInfo objectForKey:@"title"]];
        [photo setFullItemUrl:[photoInfo objectForKey:fullItemSizeUrl]];
        [photo setThumbUrl:[photoInfo objectForKey:@"url_sq"]];
        [photo setLatitude:[[photoInfo objectForKey:@"latitude"] doubleValue]];
        [photo setLongitude:[[photoInfo objectForKey:@"longitude"] doubleValue]];
        [photo setFullItemPath:[self createItemPath:photoInfo isThumbnail:NO]];
        [photo setThumbPath:[self createItemPath:photoInfo isThumbnail:YES]];
        //trigger thumbnail download
        [self downloadThumbnail:photo];
        [FMPhotoMap setObject:photo forKey:photoId];
    }
    
    //notify the datasource about new data
    NSMutableDictionary* info = [[NSMutableDictionary alloc] initWithCapacity:2];
    BOOL appendToDatasource = page > 1 && pages > 1;
    [info setObject:[NSNumber numberWithBool:appendToDatasource] forKey:@"append"];
    [info setObject:FMPhotoMap forKey:@"data"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NEW_PHOTOS_METADATA_NOTIFICATION object:nil userInfo:info];
}

@end
