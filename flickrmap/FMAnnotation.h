//
//  FMAnnotation.h
//  flickrmap
//
//
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface FMAnnotation : NSObject<MKAnnotation>
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *subtitle;
@property NSString* thumbPath;
@property NSString* fullItemPath;
@property NSString* fullItemUrl;
@property NSString* itemId;

@end
