//
//  FMDatasource.m
//  flickrmap
//
//
//

#import "FMDatasource.h"
#import "constants.h"
#import "FMNetworkManager.h"

@implementation FMPhoto
@synthesize title;
@synthesize thumbPath;
@synthesize fullItemPath;
@synthesize fullItemUrl;
@synthesize latitude;
@synthesize longitude;

-(FMAnnotation *)createAnnotationForPhotoId:(NSString*)photoId {
    FMAnnotation* ann = [[FMAnnotation alloc] init];
    CLLocationCoordinate2D coords = CLLocationCoordinate2DMake(self.latitude, self.longitude);
    [ann setCoordinate:coords];

    BOOL isDir;
    if([[NSFileManager defaultManager] fileExistsAtPath:self.thumbPath isDirectory:&isDir]) {
        //only set if thumb has been downloaded
        [ann setThumbPath:self.thumbPath];
    }
    
    [ann setFullItemPath:self.fullItemPath];
    [ann setFullItemUrl:self.fullItemUrl];
    [ann setItemId:photoId];
    return ann;
}

@end

@interface FMDatasource()
@property (strong) NSMutableDictionary* data; //key:photoid value:FMPhoto
@end

@implementation FMDatasource
- (instancetype)init; {
    if(self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newMetadataCallback:) name:NEW_PHOTOS_METADATA_NOTIFICATION object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newThumbnailCallback:) name:NEW_THUMBNAIL_NOTIFICATION object:nil];

        self.data = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) newThumbnailCallback:(NSNotification*)notification {
   
    NSDictionary* info = [notification userInfo];
    NSString* photoId = [info objectForKey:@"id"];
    FMPhoto* photo = [self.data objectForKey:photoId];
    FMAnnotation* ann = [photo createAnnotationForPhotoId:photoId];
    if(self.delegate) {
        [self.delegate drawThumbnail:ann];
    }
}

- (void) newMetadataCallback:(NSNotification*)notification {
    NSDictionary* info = [notification userInfo];
    BOOL appendData = [[info objectForKey:@"append"] boolValue];
    NSDictionary* data = [info objectForKey:@"data"];
    if(!appendData) {
        [self.data removeAllObjects];
    }
    [self.data addEntriesFromDictionary:data];
    
    //notify the UI - even if thumbs download is not complete we can still show pins on the map
    if(self.delegate) {
        [self.delegate drawAllVisibleThumbs];
    }
}

-(NSArray*) getPhotos {
    return [self.data allValues];
}

-(void)dropAllItems {
    [self.data removeAllObjects];
}

@end
