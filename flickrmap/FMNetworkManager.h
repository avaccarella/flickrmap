//
//  FMNetworkManager.h
//  flickrmap
//
//
//

#import <Foundation/Foundation.h>
#import <objectiveflickr/ObjectiveFlickr.h>
#import "FMDatasource.h"


extern NSString* const apikey;
extern NSString* const apisecret;
extern NSString* const fullItemSizeUrl;


@interface FMNetworkManager : NSObject<OFFlickrAPIRequestDelegate, NSURLSessionDelegate>
+ (FMNetworkManager*) getInstance;
- (void) downloadThumbnail:(FMPhoto*)photo;
- (void) downloadFullItem:(FMPhoto*)photo;
- (void) requestMetadataForBoundingBox:(NSString*)bbox;
@property NSURLSession* nsUrlSession;
@property (copy) NSString* lastBbox;

@end
