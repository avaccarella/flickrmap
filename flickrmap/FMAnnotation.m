//
//  FMAnnotation.m
//  flickrmap
//
//
//

#import "FMAnnotation.h"

@implementation FMAnnotation

@synthesize title;
@synthesize subtitle;
@synthesize coordinate;
@synthesize thumbPath;
@synthesize fullItemUrl;
@synthesize fullItemPath;
@synthesize itemId;

-(void)setCoordinate:(CLLocationCoordinate2D)newCoordinate {
    coordinate = newCoordinate;
}

@end
