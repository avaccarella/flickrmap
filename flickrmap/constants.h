//
//  constants.h
//  flickrmap
//
//
//

#ifndef flickrmap_constants_h
#define flickrmap_constants_h

#define NEW_PHOTOS_METADATA_NOTIFICATION    @"newphotosmetadata"
#define NEW_THUMBNAIL_NOTIFICATION          @"newthumbnail"
#define NEW_FULL_ITEM_NOTIFICATION          @"newfullitem"

#endif
