//
//  ThumbsViewCollectionViewController.m
//  flickrmap
//
//
//

#import "ThumbsViewCollectionViewController.h"
#import "FMNetworkManager.h"
#import "constants.h"
#import "PictureViewController.h"


@implementation ThumbsViewCollectionViewController

@synthesize items;

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Register cell classes
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    NSArray* itemsId = [self.items allKeys];
    NSString* itemId = [itemsId objectAtIndex:indexPath.item];
    NSDictionary* pathsForItem = [self.items objectForKey:itemId];
    
    NSString* thumbPath     = [pathsForItem objectForKey:@"thumbpath"];
    UIImage* thumb          = [[UIImage alloc] initWithContentsOfFile:thumbPath];
    UIImageView* thumbView  = [[UIImageView alloc] initWithImage:thumb];
    [cell.contentView addSubview:thumbView];
    
    return cell;
}

#pragma mark <UICollectionViewDelegateFlowLayout>
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    return CGSizeMake(80.0, 80.0);
}

#pragma mark <UICollectionViewDelegate>

-(void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray* itemsId = [self.items allKeys];
    NSString* itemId = [itemsId objectAtIndex:indexPath.item];
    NSDictionary* pathsForItem = [self.items objectForKey:itemId];
    
    NSString* itemPath = [pathsForItem objectForKey:@"itempath"];
    NSString* itemUrl = [pathsForItem objectForKey:@"itemurl"];
    FMPhoto* photo = [[FMPhoto alloc] init];
    
    [photo setFullItemPath:itemPath];
    [photo setFullItemUrl:itemUrl];
    [photo setPhotoId:itemId];
    
    PictureViewController* controller = (PictureViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"pictureviewcontrollerid"];
    
    controller.photo = photo;
    [self presentViewController:controller animated:YES completion:nil];
}


@end
