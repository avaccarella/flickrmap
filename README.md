# README #

## **Quick summary** ##
FlickrMap is a sample app to demonstrate the use of flickr REST api.
Public Flickr pictures are shown on a map where they have been geo tagged.

## ** How do I get set up? ** ##
This project makes use of Cocoapods to manage dependencies.
Run "pod install" from root folder to install third party libraries

## **Assumptions and comments** ##

* No requirement was specified about supported device orientation: orientation is resticted to portrait for the sake of simplicity

* No requirement was specified on how to render hundreds of thumbnails on the map: a clustering strategy was adopted, meaning that every pin on the map represents potentially more than one picture. Tapping on a marker a custom callout is shown with a scrollable row of picture thumbnails relevant to that area. Tapping on a thumbnail a full screen preview is opened. 

### **Issues** ###
* Getting back to the map view from the full screen preview there is a UI defect. Further investigation on the WYPopoverController third party component is needed.

* Browsing the map results in heavy memory consumption: this happens also if the flickr interaction is disabled. Further evaluation of the MKMapView is needed. Google Maps API can be considered as an alternative. All tests have been run on iOS simulator, evaluation on a real device is needed.

* Missing a cache manager to periodically purge thumbnails and full items from disk

## **Third Party Libraries** ##

All of the following were retrieved and integrated in the project using Cocoapods.

**ObjectiveFlickr**  
License: MIT  
Description: Unofficial Flickr iOS sdk  
Used for: issuing api network request  

**CCHMapClusterController**  
License: MIT  
Description: map view pins clustering logic  
Used for: displaying many annotations on an MKMapView as a unique marker  

**WYPopoverController (v0.2.2)**  
License: MIT  
Description: utility for the presentation of content in popover (both iphone and ipad)  
Used for: implementing a custom callout with a scrollable row of thumbnails